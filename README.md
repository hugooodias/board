Teste: Board
================

Ruby on Rails
-------------

- Ruby 2.3.0
- Rails 4.2.6

Getting Started
---------------
- `bundle Install`
- `rails s`

Documentation and Support
-------------------------

Issues
-------------

Similar Projects
----------------

Contributing
------------

Credits
-------

License
-------
