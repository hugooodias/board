Rails.application.routes.draw do
  get 'tasks/create'

  get 'tasks/update'

  get 'tasks/destroy'

  root to: 'boards#index'

  devise_for :users
  resources :users

  resources :boards do
    resources :tasks do
      member do
        post 'update_status'
      end
    end
  end
end
