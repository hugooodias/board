FactoryGirl.define do
  factory :task do
    title Faker::Name.name
    user nil
    board nil
  end
end
