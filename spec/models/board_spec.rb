describe Board do

  before(:each) { @user = FactoryGirl.build(:user) }

  it "is valid with a title and user" do
    board = FactoryGirl.build(:board, user: @user)
    expect(board).to be_valid
  end

  it "is invalid without a title" do
    board = FactoryGirl.build(:board, title: nil, user: @user)
    board.valid?
    expect(board.errors[:title]).to include("can't be blank")
  end

  it "is invalid without a user" do
    board = FactoryGirl.build(:board)
    board.valid?
    expect(board.errors[:user]).to include("can't be blank")
  end

  it "is private by default" do
    board = FactoryGirl.build(:board, user: @user)
    expect(board.public?).to be false
  end
end
