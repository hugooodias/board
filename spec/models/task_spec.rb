describe Task do

  before(:each) {
    @user = FactoryGirl.build(:user)
    @board = FactoryGirl.build(:board, user: @user)
  }

  it "is valid with a title, user and a board" do
    task = FactoryGirl.build(:task, board: @board, user: @user)
    expect(task).to be_valid
  end

  it "is invalid without a title" do
    task = FactoryGirl.build(:task, board: @board, user: @user, title: nil)
    task.valid?
    expect(task.errors[:title]).to include(I18n.t('errors.messages.blank'))
  end

  it "is invalid without a user" do
    task = FactoryGirl.build(:task, board: @board, user: nil)
    task.valid?
    expect(task.errors[:user]).to include(I18n.t('errors.messages.blank'))
  end

  it "is invalid without a board" do
    task = FactoryGirl.build(:task, board: nil, user: @user)
    task.valid?
    expect(task.errors[:board]).to include(I18n.t('errors.messages.blank'))
  end

  it "has todo status by default" do
    task = FactoryGirl.build(:task, board: @board, user: @user)
    expect(task.todo?).to be true
  end

  it "can be changed to in progress" do
    task = FactoryGirl.build(:task, board: @board, user: @user)
    task.inprogress!
    expect(task.inprogress?).to be true
  end

  it "can be changed to toverify" do
    task = FactoryGirl.build(:task, board: @board, user: @user)
    task.toverify!
    expect(task.toverify?).to be true
  end

  it "can be changed to done" do
    task = FactoryGirl.build(:task, board: @board, user: @user)
    task.done!
    expect(task.done?).to be true
  end

  it "has low priority by default" do
    task = FactoryGirl.build(:task, board: @board, user: @user)
    expect(task.low?).to be true
  end
end
