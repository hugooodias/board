# Feature: Home page
#   As a visitor
#   I want to visit a home page
#   So I can see the public boards
feature 'Home page' do

  # Scenario: Visit the home page
  #   Given I am a visitor
  #   When I visit the home page
  #   Then I see "My First Board"
  scenario 'visit the home page' do
    user = FactoryGirl.create(:user)
    public_board = FactoryGirl.create(:board, user: user, public: true, title: 'My First Board')
    visit root_path
    expect(page).to have_content public_board.title
  end

  # Scenario: Visit the home page
  #   Given I am a visitor
  #   When I visit the home page
  #   Private board shouldnt be visible
  scenario 'user not logged in cannot see private boards' do
    user = FactoryGirl.create(:user)
    private_board = FactoryGirl.create(:board, user: user, title: 'Private Board')
    visit root_path
    expect(page).to have_no_content private_board.title
  end

end
