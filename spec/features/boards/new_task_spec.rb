# Feature: Create new task
#   As a user
#   I want create a new task on my board
#   So I can delete it later
feature 'Create new task', :devise do

  # Scenario: User cannot create an empty task
  #   Given I want to create a new task
  #   When I submit the form without an title
  #   Then I see an title cant be blank message
  scenario 'user cannot create an empty task' do
    user = FactoryGirl.create(:user)
    board = FactoryGirl.create(:board, user: user)
    signin(user.email, user.password)
    visit board_path(board)
    page.find('.new_task--todo').click
    page.find('#new_task_todo input[type=submit]').click
    expect(page).to have_content "{\"title\":[\"can't be blank\"]}"
  end

  # Scenario: User can create a valid task
  #   Given I want to create a new task
  #   When I submit the form with an title
  #   Then i see the task in my board todo panel
  scenario 'user cannot create an empty task' do
    user = FactoryGirl.create(:user)
    board = FactoryGirl.create(:board, user: user)
    signin(user.email, user.password)
    visit board_path(board)
    page.find('.new_task--todo').click
    within(:css, "#new_task_todo") do
      fill_in 'Title', :with => 'My First Task'
    end
    page.find('#new_task_todo input[type=submit]').click
    page.should have_content 'My First Task'
  end
end
