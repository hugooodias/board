# Feature: Delete board
#   As a user
#   I want delete a board created by me
feature 'Delete board', :devise do

  # Scenario: User can delete his own board
  #   Given I want to delete a board created by me
  #   When I press the delete button
  #   Then I see an success deleted message
  scenario 'user can delete his own board' do
    user = FactoryGirl.create(:user)
    board = FactoryGirl.create(:board, user: user)
    signin(user.email, user.password)
    visit board_path(board)
    page.find('.delete-board').click
    expect(page).to have_content I18n.t 'activerecord.success.deleted_success'
  end
end
