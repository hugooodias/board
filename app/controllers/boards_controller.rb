class BoardsController < ApplicationController
  load_and_authorize_resource

  respond_to :html, :json

  def index
    if user_signed_in?
      @boards = Board.all
    else
      @boards = Board.public_only
    end
  end

  def show
    @tasks = @board.tasks.group_by { |t| t.status }

    @task_statuses = Task.statuses
    @task = @board.tasks.new
  end

  def new
  end

  def create
    @board = Board.new(board_params)
    @board.user = current_user
    flash[:notice] = I18n.t 'activerecord.success.success' if @board.save
    respond_with(@board)
  end

  def edit
  end

  def update
    flash[:notice] = I18n.t 'activerecord.success.success' if @board.update_attributes(board_params)
    respond_with(@board)
  end

  def destroy
    flash[:notice] = I18n.t 'activerecord.success.deleted_success' if @board.destroy
    redirect_to root_url
  end

  private

  def board_params
    params.require(:board).permit(:title, :public)
  end
end
