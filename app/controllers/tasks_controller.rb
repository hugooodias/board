class TasksController < ApplicationController
  load_and_authorize_resource
  respond_to :html

  before_action :set_board

  def create
    @board = Board.find(params[:board_id])
    @task = @board.tasks.build(task_params)
    @task.user = current_user
    if @task.save
      respond_with(@board, @task, status: 201, layout: false)
    else
      @task.errors.full_messages.each do |msg|
      flash[:alert] = msg
      end
      render json: @task.errors, status: :unprocessable_entity
    end
  end

  def update_status
    @task.update_attributes(task_update_status_params)
    respond_with @board, @task
  end

  def destroy
    flash[:notice] = I18n.t 'activerecord.success.deleted_success' if @task.destroy
    respond_with @board
  end

  private

  def set_board
    @board = Board.find(params[:board_id])
  end

  def task_update_status_params
    params.require(:task).permit(:status)
  end

  def task_params
    params.require(:task).permit(:title, :status, :priority)
  end
end
