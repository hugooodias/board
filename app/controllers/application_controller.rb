class ApplicationController < ActionController::Base
  rescue_from CanCan::AccessDenied do |exception|
    if user_signed_in?
      respond_to do |format|
        format.json { head :forbidden }
        format.html { redirect_to root_url, :alert => exception.message }
      end
    else
      respond_to do |format|
        format.json { head :forbidden }
        format.html { redirect_to new_user_session_path, :alert => exception.message }
      end
    end
  end

  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
end
