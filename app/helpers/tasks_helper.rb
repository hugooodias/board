module TasksHelper
  def beautify_priority(priority)
    color = nil
    case priority
    when 'low'
      color = 'info'
    when 'normal'
      color = 'success'
    when 'high'
      color = 'warning'
    when 'critical'
      color = 'danger'
    end

    "<span class='label label-#{color}'>#{priority}<span>".html_safe
  end
end
