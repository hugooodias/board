class Board
  constructor: ->
    @bind()
    @enableDraggable()

  hideForm: (status) ->
    $("#new_task_#{status}").parent().hide()

  resetForm: (status) ->
    $("#new_task_#{status}")[0].reset()

  showForm:  ->
    $(this).parent().parent().find('.card--task-form').toggle().find('input[type=text]').focus()

  updateTaskStatus: (board_id, task_id, new_status) ->
    $.post("/boards/#{board_id}/tasks/#{task_id}/update_status",
      task:
        status: new_status)
      .done (data) ->
        console.log data
      return

    $.post

  enableDraggable: ->
    tilt_direction = (item) ->
      left_pos = item.position().left

      move_handler = (e) ->
        if e.pageX >= left_pos
          item.addClass 'right'
          item.removeClass 'left'
        else
          item.addClass 'left'
          item.removeClass 'right'
        left_pos = e.pageX
        return

      $('html').bind 'mousemove', move_handler
      item.data 'move_handler', move_handler
      return

    $('.tasks_list').sortable
      connectWith: '.tasks_list'
      handle: '.task-handler'
      cancel: '.portlet-toggle'
      start: (event, ui) ->
        ui.item.addClass 'tilt'
        tilt_direction ui.item
        return
      stop: (event, ui) =>
        ui.item.removeClass 'tilt'
        $('html').unbind 'mousemove', ui.item.data('move_handler')
        ui.item.removeData 'move_handler'

        $el = $(event.toElement).parent()

        if $el.parent().hasClass 'card'
          console.log 'parent is card'
          new_status = $el.parent().parent().data 'status'
          task_id = $el.parent().data 'id'
          board_id = $el.parent().data 'board-id'
        else
          new_status = $el.parent().data 'status'
          task_id = $el.data 'id'
          board_id = $el.data 'board-id'

        @updateTaskStatus(board_id, task_id, new_status)

        return


    $('.task-card').addClass('ui-widget ui-widget-content ui-helper-clearfix ui-corner-all').find('.task-handler').addClass 'ui-widget-header ui-corner-all'

  bind: ->
    $(document).on 'click', '.new_task_btn', @showForm

    $(".simple_form.new_task").on("ajax:success", (e, data, status, xhr) =>
      $el = $('<div/>').html(data).contents()
      status = $el.data 'status'

      $(".panel-statuses-#{status} .tasks_list").prepend $el

      @resetForm status
      @hideForm status

      toastr.success('Yay!', 'Task created')

    ).on "ajax:error", (e, xhr, status, error) ->
      window.location.reload()

$ ->
  new Board()
