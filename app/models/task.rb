class Task < ActiveRecord::Base
  enum status: [:todo, :inprogress, :toverify, :done]
  enum priority: [:low, :normal, :high, :critical]

  belongs_to :user
  belongs_to :board

  validates_presence_of :user
  validates_presence_of :board
  validates_presence_of :title
end
