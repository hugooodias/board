class Ability
  include CanCan::Ability

  def initialize(user)
    user ||= User.new

    if user.id?
      can :manage, Board, user_id: user.id
      can :manage, Task

      can :read, Board
    else # Guest
      can :read, Board, :public => true
      can :read, Task
    end
  end
end
