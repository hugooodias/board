class Board < ActiveRecord::Base
  scope :public_only, -> { where(:public => true) }

  belongs_to :user
  has_many :tasks

  validates_presence_of :title
  validates_presence_of :user
end
